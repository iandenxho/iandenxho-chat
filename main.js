import Vue from 'vue';
import ChatMain from './src/components/ChatMain';
import './src/assets/js/filters/filters-date';
import store from './src/store';

new Vue({
	el: '#chat',
    components: {
    	ChatMain
    },
	store
});
