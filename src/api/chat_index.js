import {
    sendChatThreadMessage as apiSendChatThreadMessage
} from "../api/resources";
import * as messageStatusTypes from '../store/message-status-types';



export function createMessage ({ text, thread, loggedUserId, profilePicture }, cb) {
    const timestamp = Math.floor(Date.now() / 1000);
    const threadId = thread.id;
    const id = 'tmp_' + timestamp;
    const message = {
        id,
        sender: 'self',
        message: text,
        img_url: profilePicture,
        date_timestamp: timestamp,
        status: messageStatusTypes.SENDING,
        seen_by: [],
        threadId: threadId,
        authorId: loggedUserId,

    };

    cb(message);

    let data = {
        userId: loggedUserId,
        threadId: threadId,
        text: text,
        tempId: id
    };

    apiSendChatThreadMessage(data, ({responseData}) => {
        message.status = messageStatusTypes.SENT;
        message.id = responseData.id;
        message.date_timestamp = responseData.date_timestamp;
        cb(message);
    }, ({}) => {
        message.status = messageStatusTypes.FAILED_TO_SEND;
        cb(message);
    });
}
