import api from '../index'

export function getChatThreads(data, successHandler, errorHandler = null) {
    // data = {
    //      userId: string,
    //      startPage: string,
    //      endPage: string,
    //      limit: string
    //      searchFilter: string
    // };
    api.post(`/chat/threads`, data, successHandler, errorHandler);
}

export function getChatThreadMessages(userId, threadId, startPage, endPage, limit, successHandler, errorHandler = null) {
    api.get(`/chat/thread-messages/${userId}/${threadId}/${startPage}/${endPage}/${limit}`, successHandler, errorHandler);
}

export function sendChatThreadMessage(data, successHandler, errorHandler = null) {
    // data = {
    //      userId: string,
    //      threadId: string,
    //      text: string,
    //      tempId: string
    // };
    api.post(`/chat/thread-messages/send`, data, successHandler, errorHandler);

}

export function setMessagesAsRead (data, successHandler, errorHandler = null) {
    // data = {
    //     threadId: string,
    //     messageIDs: array,
    //     userId: string,
    // };
    api.post('/chat/thread-messages/mark-as-seen', data, successHandler, errorHandler);
}

export function getUnreadMessagesCount(userId, successHandler, errorHandler = null) {
    api.get(`/chat/thread-messages/unread-count/${userId}`, successHandler, errorHandler);
}

export function createGroupChat (data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     threadName: string,
    //     participants: array,
    // };
    api.post('/chat/create-thread', data, successHandler, errorHandler);
}

export function getUserOptionsForGroup (data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     filter: string,
    // };
    api.post('/chat/get-user-options', data, successHandler, errorHandler);
}

export function removeUsersFromThread (data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     threadId: string,
    //     threadParticipantIDs: string,
    // };
    api.post('/chat/remove-users-from-thread', data, successHandler, errorHandler);
}

export function getThreadUsers (data, successHandler, errorHandler = null) {
    // data = {
    //     threadId: string,
    //     filter: string,
    // };
    api.post('/chat/get-thread-users', data, successHandler, errorHandler);
}

export function updateThreadName (data, successHandler, errorHandler = null) {
    // data = {
    //     threadId: string,
    //     newName: string,
    // };
    api.post('/chat/update-thread-name', data, successHandler, errorHandler);
}

export function broadcastStatus (data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string,
    //     status: string,
    // };
    api.post('/chat/broadcast/status', data, successHandler, errorHandler);
}

export function getOnlineUsers (data, successHandler, errorHandler = null) {
    // data = {
    //     userId: string
    // };
    api.post('/chat/online-users', data, successHandler, errorHandler);
}