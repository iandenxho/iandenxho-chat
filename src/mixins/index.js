import * as messageStatusTypes from '../store/message-status-types.js';
import * as types from '../store/mutation-types';

export default {
	methods: {
		isThreadTypeDirect(thread) {
			return thread.thread_type === 1;
		},
		isThreadTypeGroup(thread) {
			return thread.thread_type > 1;
		},
		isHistoryLogOthers(historyLog) {
			return historyLog.sender === 'others';
		},
		isHistoryLogSelf(historyLog) {
			return historyLog.sender === 'self';
		},
		isHistoryLogStatusSeen(historyLog) {
			return historyLog.status === messageStatusTypes.SEEN;
		},
		isHistoryLogStatusSeenAll(historyLog) {
			return historyLog.status === messageStatusTypes.SEEN_BY_ALL;
		},
		isHistoryLogStatusSent(historyLog) {
			return historyLog.status === messageStatusTypes.SENT;
		},
		isHistoryLogStatusSending(historyLog) {
			return historyLog.status === messageStatusTypes.SENDING;
		},
		isHistoryLogStatusRetry(historyLog) {
			return historyLog.status === messageStatusTypes.FAILED_TO_SEND;
		},
		closeChatbox() {
			this.$root.$emit('on::chatbox::close');
		},
		scrollToBottom(el) {
			if(el !== undefined) {
				el.scrollTop = el.scrollHeight;
			}
		},
		showToastSuccess(title, message) {
			if(common !== undefined) {
				common.showToastSuccess(title, message);
				return;
			}
			// Toaster Fallback
			alert(`${title}: ${message}`)
		},
		showToastError(title, message) {
			if(common !== undefined) {
				common.showToastError(title, message);
				return;
			}
			// Toaster Fallback
			alert(`${title}: ${message}`)
		}
	}
}