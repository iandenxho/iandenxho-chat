import {
    createMessage as apiCreateMessage
} from '../api/chat_index';
import * as types from './mutation-types';
import * as messageStatusTypes from './message-status-types';

export const sendMessage = ({ commit }, payload) => {
    apiCreateMessage(payload, message => {
        message.sender = 'self';
        commit(types.RECEIVE_MESSAGE, { message: message, isNew: true })
    });
};

export const receiveMessage = ({ commit }, payload) => {

    let messageStatus = messageStatusTypes.SEEN;

    if (payload.fromMe) {
        messageStatus = messageStatusTypes.SENT;
    }

    const message = {
        id: payload.message_id,
        message: payload.message,
        timestamp: payload.timestamp,
        threadId: payload.threadId,
        threadName: payload.threadName,
        threadAvatar: payload.authorAvatar,
        authorName: payload.name,
        threadType: payload.threadType,
        type: payload.type,
        subscribers: payload.subscribers,
        authorId: payload.authorId,
        authorAvatar: payload.authorAvatar,
        fromMe: payload.fromMe,
        fromSeller: payload.fromSeller,
        status: messageStatus,
    };

    commit(types.RECEIVE_MESSAGE, { message: message, isNew: true });
};
