import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'
import createLogger from '../assets/js/plugins/logger';


Vue.use(Vuex);

const state = {
	canAddGroups: false,
	canManageGroups: false,
	loggedUserId: null,
	loggedUserPhoto: null,
	totalUnreadMessagesCount: 0,
	isChatOpen: false,
	isLoadingHistory: false,
	showMessageSection: false,
	currentThreadKey: null,
	currentThreadID: null,
	typingTimeout: 900,
	participants: [],
	onlineUsers: [],
	userObject: {},
	// TODO: Get/Load data from API
	threads: [],
	messages: [],
	members: [],
	membersSelected: [],
	gcvName: null,
	gccName: null,
	gccSearchFilter: null,
	gccUsers: [],
	gccSelectedUserIDs: []
};

export default new Vuex.Store({
	state,
	getters,
	actions,
	mutations,
	plugins: process.env.NODE_ENV !== 'production'
		? [createLogger()]
		: []
});
