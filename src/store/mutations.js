import * as types from './mutation-types';
import * as messageStatusTypes from '../store/message-status-types.js';
import {
    setMessagesAsRead as apiSetMessagesAsRead
} from "../api/resources";


export default {
    [types.SHOW_MESSAGE_SECTION] (state, toggle) {
        state.showMessageSection = toggle;
    },

    [types.SET_COMPACT_CHAT_VISIBILITY] (state, isChatOpen) {
        state.isChatOpen = isChatOpen;
    },

    [types.SWITCH_THREAD] (state, { key }) {
        state.isLoadingHistory = true;
        setCurrentThread(state, key);
        state.showMessageSection = true;
    },
    [types.SET_TOTAL_UNREAD_COUNT] (state, totalUnreadMessagesCount) {
        state.totalUnreadMessagesCount = totalUnreadMessagesCount;
    },

    [types.INITIALIZE_USER_ID] (state, userId) {
        state.loggedUserId = userId;
    },

    [types.INCREMENT_TOTAL_UNREAD_COUNT] (state) {
        state.totalUnreadMessagesCount = state.totalUnreadMessagesCount + 1;
    },

    [types.SET_USER_PROFILE_PHOTO] (state, loggedUserPhoto) {
        state.loggedUserPhoto = loggedUserPhoto;
    },

    [types.SET_THREADS] (state, threads) {
        state.threads = threads;
        sortThreads(state);
    },

    [types.SET_MESSAGES] (state, messages) {
        state.isLoadingHistory = false;
        state.messages = messages;
    },

    [types.MARK_MESSAGES_AS_SEEN] (state, messageIDs) {
        if (state.currentThreadKey != null) {
            messageIDs.forEach(function (messageID) {
                let messageKey = getMessageKeyById(state, messageID);
                if (messageKey >= 0) {
                    state.messages[messageKey].status = messageStatusTypes.SEEN;
                }
            });
        }
    },

    [types.SET_THREAD_HAS_UNREAD] (state, threadId) {
        setThreadHasUnread(state, threadId);
    },

    [types.RESET_THREAD_HAS_UNREAD] (state, threadId) {
        resetThreadHasUnread(state, threadId);
    },

    [types.RECEIVE_MESSAGE] (state, { message, isNew }) {
        addMessage(state, message, isNew);
        sortThreads(state);
    },

    [types.RECEIVE_BROADCAST_MESSAGE] (state, { data, isNew }) {
        appendMessage(state, data, isNew);
        sortThreads(state);
    },

    [types.REMOVE_FROM_THREAD_MESSAGES] (state, { failedThreadMessage }) {
        state.messages = state.messages.filter(function (threadMessage) {
            return threadMessage.id !== failedThreadMessage.id;
        });
    },

    [types.SET_GCC_USERS] (state, users) {
        state.gccUsers = users;
    },

    [types.SET_SELECTED_GCC_USERS] (state, selectedUserIDs) {
        state.gccSelectedUserIDs = selectedUserIDs;
    },

    [types.SET_GROUP_CHAT_NAME] (state, name) {
        state.gccName = name;
    },

    [types.RESET_GROUP_CHAT_CREATOR] (state) {
        state.gccName = null;
        state.gccSelectedUserIDs = [];
        state.gccUsers = [];
        state.gccSearchFilter = null;
    },

    [types.ADD_THREAD] (state, thread) {
        createThread(state, thread);
        let threadKey = getThreadKeyById(state, thread.id);
        setCurrentThread(state, threadKey);
        sortThreads(state);

        if (state.isChatOpen) {
            setCurrentThread(state, threadKey);
            state.showMessageSection = true;
            state.messages = [];
        }
    },

    [types.GCC_SELECT_USER] (state, userId) {
        let gccUserKey = parseInt(
            Object.keys(state.gccUsers).find(
                key => state.gccUsers[key].id === userId)
        );

        state.gccUsers[gccUserKey].is_selected = true;
    },

    [types.GCC_DESELECT_USER] (state, userId) {
        let gccUserKey = parseInt(
            Object.keys(state.gccUsers).find(
                key => state.gccUsers[key].id === userId)
        );

        state.gccUsers[gccUserKey].is_selected = false;
    },

    [types.SET_GCC_USERS_IS_SELECTED] (state) {
        state.gccUsers.forEach(function (user) {
            user.is_selected = state.gccSelectedUserIDs.includes(user.id);
        });
    },

    [types.SET_CAN_ADD_GROUPS] (state, access) {
        state.canAddGroups = access == 1;
    },

    [types.SET_CAN_MANAGE_GROUPS] (state, access) {
        state.canManageGroups = access == 1;
    },

    [types.REMOVE_THREAD] (state, threadId) {
        state.threads = state.threads.filter(function (thread) {
            return thread.id !== threadId;
        });
        setCurrentThread(state, 0);
    },

    [types.OTHERS_LEFT_THREAD] (state, data) {
        let threadKey = getThreadKeyById(state, data.threadId);
        let thread = state.threads[threadKey];

        thread.members = thread.members.filter(function(e) {
                return this.indexOf(e.id) < 0;
            }, data.userIDs
        );

        thread.member_count = thread.members.length;
    },

    [types.UPDATE_THREAD_MEMBERS] (state, dataUpdate) {
        let threadKey = getThreadKeyById(state, dataUpdate.threadId);
        let thread = state.threads[threadKey];
        thread.members = dataUpdate.members;
    },

    [types.UPDATE_THREAD_NAME] (state, dataUpdate) {
        let threadKey = getThreadKeyById(state, dataUpdate.threadId);
        let thread = state.threads[threadKey];
        thread.thread_name = dataUpdate.newName;
    },

    [types.SET_ONLINE_USERS] (state, userIDs) {
        state.onlineUsers = userIDs;
    },

    [types.REMOVE_ONLINE_USER] (state, userId) {
        state.onlineUsers = state.onlineUsers.filter(function (value) {
            return value !== userId;
        });
    },

    [types.ADD_ONLINE_USER] (state, userId) {
        state.onlineUsers.push(userId);
    },

    [types.UPDATE_USER_NAME] (state, data) {
        updateThreadName(state, data);
    },

    [types.UPDATE_USER_AVATAR] (state, data) {
        updateThreadAvatar(state, data);
    },

    [types.SET_MEMBERS_SELECTED] (state, membersSelected) {
        state.membersSelected = membersSelected;
    },

    [types.RESET_MEMBERS_SELECTED] (state) {
        state.membersSelected = [];
    }
}

function sortThreads(state) {
    //sort the threads in descending order based on their timestamps
    state.threads = state.threads
        .slice()
        .sort((a, b) => b.date_timestamp - a.date_timestamp);

    //reset the current thread key since the threads will be re-sorted
    state.currentThreadKey = state.currentThreadID ? parseInt(
        Object.keys(state.threads).find(
            key => state.threads[key].id === state.currentThreadID)
    ) : null;
}

function createThread (state, threadData) {
    //push the new thread object and get the new index
    if (!state.threads.some(thread => thread.id === threadData.id)) {
        //push the thread if it's not yet existing

        return state.threads.push(threadData) - 1;
    }
}

function appendMessage (state, data, newMessage = false) {
    // add it to the thread it belongs to
    let thread = state.threads.find(function(thread) {
        return thread.id === data.thread.id;
    });

    if (!thread) {
        //create a new thread if message is new
        let threadData = data.thread;

        let newThreadKey = createThread(state, threadData);
        thread = state.threads[newThreadKey];
    }

    //update the thread's timestamp for sorting
    thread.date_timestamp = data.thread.date_timestamp;

    if (!state.messages.some(threadMessage => threadMessage.id === data.message.id || threadMessage.id === data.tempId)) {
        //push the message if it's not yet existing

        //check if the current thread is same with the thread of the incoming message
        //IF not, don't push to thread messages, just update the last message
        if (data.thread.id === state.currentThreadID) {
            state.messages.push(data.message);
        }

        //set the current as last message if there's none set or if it's a new message
        if (thread.recent_message === null || newMessage) {
            thread.recent_message = data.message.message;
        }
    }


    if (state.currentThreadID != null) {
        let postData = {
            threadId: state.currentThreadID,
            messageIDs: [data.message.id],
            userId: state.loggedUserId
        };

        apiSetMessagesAsRead(postData, () => {});
    }
}

function addMessage (state, message, newMessage = false) {
    // add it to the thread it belongs to
    let thread = state.threads.find(function(thread) {
        return thread.id === message.threadId;
    });

    if (!thread) {
        //create a new thread if message is new
        let threadData = {
            id: message.threadId,
            name: message.threadName,
            type: message.threadType,
            subscribers: message.subscribers,
            avatar: message.threadAvatar,
            lastMessage: message,
            timestamp: message.timestamp,
            unreadCount: 0,
            userHasLeft: false,
        };

        let newThreadKey = createThread(state, threadData);
        thread = state.threads[newThreadKey];
    }

    //update the thread's timestamp for sorting
    thread.date_timestamp = message.date_timestamp;

    if (!state.messages.some(threadMessage => threadMessage.id === message.id)) {
        //push the message if it's not yet existing

        //check if the current thread is same with the thread of the incoming message
        //IF not, don't push to thread messages, just update the last message
        if (message.threadId === state.currentThreadID) {
            state.messages.push(message);
        }

        //set the current as last message if there's none set or if it's a new message
        if (thread.recent_message === null || newMessage) {
            thread.recent_message = message.message;
        }
    }
}

function getThreadKeyById(state, threadId) {
    return parseInt(
        Object.keys(state.threads).find(
            key => state.threads[key].id === threadId)
    );
}

function getThreadKeyByOtherUserId(state, userId) {
    return parseInt(
        Object.keys(state.threads).find(
            key => state.threads[key].user_id === userId)
    );
}

function getMessageKeyById(state, messageId) {
    return parseInt(
        Object.keys(state.messages).find(
            key => state.messages[key].id === messageId)
    );
}

function updateThreadAvatar(state, data) {
    let threadKey = getThreadKeyByOtherUserId(state, data.userId);
    state.threads[threadKey].img_url = data.avatarURL;
}

function updateThreadName(state, data) {
    let threadKey = getThreadKeyByOtherUserId(state, data.userId);
    state.threads[threadKey].thread_name = data.threadName;
}

function setThreadHasUnread(state, threadId) {
    let threadKey = getThreadKeyById(state, threadId);
    if (state.currentThreadID !== threadId) {
        state.threads[threadKey].has_unread = true;
        state.threads[threadKey].unread_count++;
    }
}

function resetThreadHasUnread(state, threadId) {
    let threadKey = getThreadKeyById(state, threadId);
    state.threads[threadKey].has_unread = false;
    state.threads[threadKey].unread_count = 0;

}

function setCurrentThread (state, key) {
    state.currentThreadKey = key;
    state.currentThreadID = state.threads[key].id;
    state.threads[key].has_unread = false;
    state.threads[key].is_active = true;
}