const { VueLoaderPlugin } = require('vue-loader');
const path = require('path');

var config = {
	entry: [
		'./main.js'
	],
	output: {
		filename: 'chat.js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		rules:[
			{
				test:/\.(s*)css$/,
				use: [
					'vue-style-loader',
					'url-loader',
					'css-loader',
					'sass-loader'
				],
				include: [
					path.resolve('src')
				]
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: [
					path.resolve('src')
				]
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				include: [
					path.resolve('src')
				]
			},
			{
				test: /\.(jpe?g|png|gif)$/,
				loaders: 'file-loader'
			},
			{
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: "url-loader?limit=10000&mimetype=application/font-woff"
			},
			{
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: "file-loader"
			},
		],
	},
	resolve: {
		alias: {
			'vue$': 'vue/dist/vue.esm.js'
		},
		extensions: ['*', '.js', '.vue', '.json']
	},
	plugins: [
		new VueLoaderPlugin()
	]
};

module.exports = config;